import { runInThisContext } from "vm";




export function fizzbuzz() {
   return [...Array(100).keys()]
            .map((n) => n + 1 )
            .map(iterateFizzBuzz)
            .join()

}

function iterateFizzBuzz(n: number): string | number {

    const num: string | number =    n % 15=== 0 ? 'FizzBuzz' 
                                  : n % 3 === 0 ? 'Fizz' 
                                  : n % 5 === 0 ? 'Buzz' 
                                  : n

   return num
}
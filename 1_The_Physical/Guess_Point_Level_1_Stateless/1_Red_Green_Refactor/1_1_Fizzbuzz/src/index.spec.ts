import { describe, it, expect } from '@jest/globals'
import { fizzbuzz } from './fizzbuzz'

describe("fizzbuzz", () => {
    it('fizz buzz returns string', () => {
        expect(typeof fizzbuzz()).toBe('string')
    })

    it('generates number 1 to 100', () => {
        let retValue: string = fizzbuzz()
        expect(retValue.split(',').length).toBe(100)
    })

    it('Non Multiples of 3 and 5 returns number', () => {
        let retValue: string = fizzbuzz()
        expect(retValue.split(',')[0]).toBe('1') 
        expect(retValue.split(',')[1]).toBe('2') 
        expect(retValue.split(',')[12]).toBe('13') 
    })

    it('Multiples of 3 returns Fizz', () => {
        let retValue: string = fizzbuzz()
        expect(retValue.split(',')[2]).toBe('Fizz') 
        expect(retValue.split(',')[5]).toBe('Fizz') 
    })

    it('Multiples of 5 returns Buzz', () => {
        let retValue: string = fizzbuzz()
        expect(retValue.split(',')[4]).toBe('Buzz') 
        expect(retValue.split(',')[9]).toBe('Buzz') 
    })

    it('Multiples of 15 returns FizzBuzz', () => {
        let retValue: string = fizzbuzz()
        expect(retValue.split(',')[14]).toBe('FizzBuzz') 
        expect(retValue.split(',')[29]).toBe('FizzBuzz') 
    })


});
